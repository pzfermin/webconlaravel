

<header class="nav-container">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark headroom animated fixed-top">
        <!-- <a class="navbar-brand" href="index.html">BA-Developers</a>-->
        <img src="{{asset('images/fermin.png')}}" alt="logoFermin" id="logoFermin">
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"></span>
         </button>
         <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
             <ul class="navbar-nav">
                 <li class="nav-item">
                     <a class="nav-link" href="#home">Home</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="#proyecto">Sobre mi</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="#propuesta">Quien soy?</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="#Contacto">Contactame</a>
                 </li>
             </ul>
         </div>
     </nav>  

<div class="jumbotron img-resposive  align-self-center " id="home">
    <div class="container">
        <div class="row align-self-center">
            <div class="col-sm-4 col-xs-4"></div>
            <img class="col-md-4 col-xs-4" max-width="200px" id="Logo" src="{{asset('images/fermin.png')}}" alt="Logo Fermin">
            <div class="col-sm-4 col-xs-4"></div>
        </div>
    </div> 
    <div class="container"> 
        <div class="row align-self-center">
            <div class="col-sm-2"></div>
            <h1 class="lead col-sm-8 centrar" id="creative-web">Hacer Webs Creativas</h1>
            <div class="col-sm-2"></div>
        </div>    
    </div>  
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <h2 class="col-sm-4 centrar">No es un trabajo. Es mi diversion!</h2>
            <div class="col-sm-4"></div>
        </div>
    </div>
   <div class="container">
       <div class="row">
            <div class="col-md-5 col-sm-4"></div>
            <a class="col-md-2 col-sm-4 btn btn-outline-warning" href="#" role="button" id="learn-more">Leer mas!</a>       
            <div class="col-md-5 col-sm-4"></div>
        </div>
   </div>  
</div>          
</header>