<?php
    require_once("head.php");
    require_once("header.php");
?>

<div class="gohome">
    <div class="row">
        <div class="col-sm-4"></div>
        <h2 class="col-sm-4 gracias" id="gracias">Gracias por contactarte!</h2>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-4"></div>
        <button type="button" class="col-sm-4 btn btn-outline-warning">Go Home!</button>
        <div class="col-sm-4"></div>
    </div>
</div>

<?php
    require_once("footer.php");
?>