<footer>
              <!-- Footer Elements -->
    <div class="container container-footer">

        <!-- Grid row-->
        <div class="row">
  
            <!-- Grid column -->
            <div class="col-md-12 py-5">
                <div class="mb-5 flex-center">
    
                <!-- Facebook -->
                <a href="https://www.facebook.com/fermin.paez" class="fb-ic">
                    <i class="fa fa-facebook fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </a>
                <!-- Twitter -->
                <a href="#"class="tw-ic">
                    <i class="fa fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </a>
                <!-- Google +-->
                <a href="https://plus.google.com/u/0/106559520185752914412" class="gplus-ic">
                    <i class="fa fa-google-plus fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </a>
                <!--Linkedin -->
                <a href="https://www.linkedin.com/in/fermin-paez/"class="li-ic">
                    <i class="fa fa-linkedin fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </a>
                <!--Instagram-->
                <a href="https://www.instagram.com/hey_fer_do/?hl=es-la"class="ins-ic">
                    <i class="fa fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </a>
                <!--Pinterest-->
                <a href="https://ar.pinterest.com/pzfermin/" class="pin-ic">
                    <i class="fa fa-pinterest fa-lg white-text fa-2x"> </i>
                </a>
                </div>
            </div>
          <!-- Grid column -->
  
        </div>
        <!-- Grid row-->
  
      </div>
      <!-- Footer Elements -->
  
      <!-- Copyright -->
      <div class=" text-center py-3" id="Copyright">© 2019 Copyright:
        <a href="#" id="go-home"> FerminPaez.com</a>
      </div>
      <!-- Copyright -->
  
    </footer>
    <!-- Footer -->