@include('head');
@include('header')

<main>
    <div class="container">
        <div class="row">
            
            <h1 class="" id="titulo-servicios">Servicios</h1>
            
        </div>
        <ul class="row">
            <li class="col-sm-4 columna-servicios">
                <h2>Precencia Web</h2>
                <p>La precencia web es el punto de partida para toda organizacion. Esta pagina, aunque simple, le ayudara a hacerce conocer. </p>
                <div class="row">
                    <div class="col-sm-3"></div>
                        <a class="col-sm-6 btn btn-outline-warning">Seleccionar!</a>
                    <div class="col-sm-3"></div>
                </div>
            </li>
            <li class="col-sm-4 columna-servicios">
                <h2>Tu Web Crece</h2>
                <p>A medida que la organizacion comienza a crecer es necesario agregar mayor cantidad de paginas dentro de la web.</p>
                <div class="row">
                    <div class="col-sm-3"></div>
                        <a class="col-sm-6 btn btn-outline-warning">Seleccionar!</a>
                    <div class="col-sm-3"></div>
                </div>
            </li>
            <li class="col-sm-4 columna-servicios">
                <h2>E-Comerce</h2>
                <p>Con esta opcion usted podra vender sus productos o servicios al mundo.</p>
                <div class="row">
                    <div class="col-sm-3"></div>
                        <a class="col-sm-6 btn btn-outline-warning" id="seleccionar">Seleccionar!</a>
                    <div class="col-sm-3"></div>
                </div>
            </li>
        </ul>
    </div>
</main>
    
@include('footer')